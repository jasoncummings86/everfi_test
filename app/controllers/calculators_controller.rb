class CalculatorsController < ApplicationController
  def compute
    respond_to do |format|
      format.js do
        result = Calculator.calculate(calculator_params[:expression])
        render js: "Calculator.appendResult('#{result}')"
      end
    end
  end

  def calculator_params
    params.require(:calculator).permit(:expression)
  end
end
