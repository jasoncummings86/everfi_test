class Calculator
  class << self
    INTEGERS_AND_OPERATORS = /\d+|[\+\*\-\/]/
    INVALID_TOKENS = /[a-zA-z]/

    def calculate(expression)
      invalid_tokens = expression.scan(INVALID_TOKENS)
      valid_tokens = expression.scan(INTEGERS_AND_OPERATORS)

      return 'Please enter numbers and operators' if invalid_tokens.any?

      return valid_tokens[0].to_f.round(2) if valid_tokens.length == 1

      intermediate_result = compute_multiplication_and_division!(valid_tokens)

      return intermediate_result.round(2) if intermediate_result.is_a?(Numeric)

      result = compute_subtraction_and_addition!(valid_tokens)
      result.round(2)
    end

    private

    def compute_subtraction_and_addition!(tokens)
      tokens.each.with_index do |token, idx|
        if token == '+' || token == '-'
          tokens = evaluate_found_operation(tokens, idx)

          if tokens.length == 1
            return tokens[0]
          end

          break
        end
      end

      compute_subtraction_and_addition!(tokens)
    end

    def compute_multiplication_and_division!(tokens)
      end_of_expression = true

      tokens.each.with_index do |token, idx|
        if token == '*' || token == '/'
          end_of_expression = false

          tokens = evaluate_found_operation(tokens, idx)
          end_of_expression = tokens.length - 1 == idx

          if tokens.length == 1
            return tokens[0]
          end

          break
        end
      end

      if end_of_expression
        tokens
      else
        compute_multiplication_and_division!(tokens)
      end
    end

    def evaluate_found_operation(tokens, idx)
      left_operand_idx = idx - 1
      right_operand_idx = idx + 1

      result = tokens[left_operand_idx].to_f.send(
        tokens[idx],
        tokens[right_operand_idx].to_f
      )

      tokens[right_operand_idx] = result
      tokens.slice!(left_operand_idx..idx)
      tokens
    end
  end

end
