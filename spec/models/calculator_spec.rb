require 'rails_helper'

RSpec.describe Calculator do
  describe '.calculate' do
    it 'returns an error message if it detects non digit word chars' do
      result = described_class.calculate('+ a')
      expect(result).to eq('Please enter numbers and operators')
    end

    it 'returns the integer if an integer is submitted' do
      result = described_class.calculate('42')
      expect(result).to eq(42)
    end

    it 'handles multiplication and division' do
      result = described_class.calculate('8 / 4 * 3 * 7')
      expect(result).to eq(42)
    end

    it 'handles addition and subtraction' do
      result = described_class.calculate('5 + 2 - 3 - 8')
      expect(result).to eq(-4)
    end

    it 'correctly handles order of operations' do
      result = described_class.calculate('2 + 4 * 3')
      expect(result).to eq(14)
    end

    it 'correctly handles floating point rounding' do
      result = described_class.calculate('5*3+1+6/85+9*100')

      expect(result).to eq(916.07)
    end
  end
end
