# Everfi exercise

This was built using Rails 5.0 and ruby 2.3.4.

Typically I would do a feature spec for the view in this exercise,
but based on my phone call it sounded like the position was API heavy/exclusive,
so I figured it wouldn't be useful.
